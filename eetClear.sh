#!/bin/sh

# Manual clear of buildfiles for EET template

rm -f 00-index.aux
rm -f 00-index.bbl
rm -f 00-index.blg
rm -f 00-index.fdb_latexmk
rm -f 00-index.fls
rm -f 00-index.lof
rm -f 00-index.log
rm -f 00-index.lot
rm -f 00-index.out
rm -f 00-index.synctex\(busy\)
rm -f 00-index.synctex.gz
rm -f 00-index.toc
rm -f VIK_ff_inv.pgf
rm -f bmelogo.pgf
rm -f huhyphn.tex
rm -f magyar.ldf
